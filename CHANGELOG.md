# Alexey Sidoryak change log

## Version 1.0.0 under development

- New: Added templates for all pages
- New: Added Inter font
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init June 27, 2023
